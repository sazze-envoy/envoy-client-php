<?php
/**
 * PHP version 5
 *
 * @author Craig Thayer <cthayer@sazze.com>
 * @copyright 2014 Sazze, Inc.
 */


require __DIR__ . '/../vendor/autoload.php';

$config = new stdClass();
$config->clientType = 'http';
$config->senderDomain = 'example.com';
$config->secret = 'blah';
$config->host = '172.16.120.70';

$email = new sz\envoy\TransactionEmail();
$email->to = 'cthayer@example.com';
$email->from = 'support@example.com';
$email->subject = 'test from php (http)';
$email->text = 'test test test';

$client = new sz\envoy\SendClient($config);

$resp = $client->sendEmail($email);

echo "\nerror:\n";
print_r($client->lastError);
echo "\nresp:\n";
print_r($resp);