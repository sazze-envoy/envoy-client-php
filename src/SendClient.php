<?php
/**
 * Defines the SendClient class
 *
 * PHP version 5
 *
 * @author Craig Thayer <cthayer@sazze.com>
 * @copyright 2014 Sazze, Inc.
 */

namespace sz\envoy;

use Hashids\Hashids;

class SendClient
{
    const CLIENT_TYPE_HTTP = 'http';
    const CLIENT_TYPE_THRIFT = 'thrift';

    protected $client;
    protected $clientType;
    protected $secret;
    protected $senderDomain;

    public $lastError;

    public function __construct($config)
    {
        $this->secret = $config->secret;
        $this->senderDomain = $config->senderDomain;
        $this->lastError = null;

        switch ($config->clientType) {
            case self::CLIENT_TYPE_THRIFT:
                $this->clientType = self::CLIENT_TYPE_THRIFT;
                $this->client = new \sz\envoy\thrift\clients\SendClient($config);
                break;

            case self::CLIENT_TYPE_HTTP:
            default:
                $this->clientType = self::CLIENT_TYPE_HTTP;

                $options = [
                    'connecttimeout' => 1
                ];

                if (isset($config->httpOptions) && is_array($config->httpOptions)) {
                    $options = array_merge($options, $config->httpOptions);
                }

                $this->client = new \HttpRequest('http' . (isset($config->ssl) && $config->ssl ? 's' : '') . '://' . $config->host . '/send/email', \HTTP_METH_POST, $options);

                break;
        }
    }

    public function sendEmail(Email $email)
    {
        $req = $this->buildRequest($email, $this->secret);

        switch ($this->clientType) {
            case self::CLIENT_TYPE_THRIFT:
                try {
                    $thriftResp = $this->client->send($req->__toString());
                } catch (\Exception $e) {
                    $this->lastError = $e;
                    return false;
                }

                $resp = json_decode($thriftResp);

                break;

            case self::CLIENT_TYPE_HTTP:
            default:
                $this->client->setBody($req->__toString());
                $this->client->setHeaders(['Content-Type' => 'application/json']);

                try {
                    $httpResp = $this->client->send();
                } catch (\Exception $e) {
                    $this->lastError = $e;
                    return false;
                }

                if ($httpResp->getResponseCode() != 200) {
                    $this->lastError = $httpResp->getBody();
                    return false;
                }

                $resp = json_decode($httpResp->getBody());

                break;
        }

        if (!isset($resp->sent) || !$resp->sent) {
            $this->lastError = $resp;
            return false;
        }

        return $resp;
    }

    public function signEmail($emailStr, $secret, $timeStr)
    {
        $hashIds = new Hashids($secret);

        return md5($hashIds->encode(strtotime($timeStr)) . $emailStr);
    }

    public function buildRequest(Email $email, $secret)
    {
        $ret = new SendRequest();
        $ret->email = $email->__toString();
        $ret->time = date('r');
        $ret->sig = $this->signEmail($ret->email, $secret, $ret->time);
        $ret->senderDomain = $this->senderDomain;

        return $ret;
    }
}