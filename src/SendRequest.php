<?php
/**
 * Defines the SendRequest class
 *
 * PHP version 5
 *
 * @author Craig Thayer <cthayer@sazze.com>
 * @copyright 2014 Sazze, Inc.
 */

namespace sz\envoy;

class SendRequest
{
    public $sig = '';
    public $email = '';
    public $time = '';
    public $senderDomain = '';

    public function __toString()
    {
        return json_encode($this);
    }
}