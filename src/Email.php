<?php
/**
 * Defines the Email class
 *
 * PHP version 5
 *
 * @author Craig Thayer <cthayer@sazze.com>
 * @copyright 2014 Sazze, Inc.
 */

namespace sz\envoy;

class Email
{
    public $to = '';
    public $from = '';
    public $replyTo = '';
    public $subject = '';
    public $html = '';
    public $text = '';
    public $headers = [];

    public function __construct($jsonStr = '')
    {
        if (!$jsonStr) {
            return;
        }

        $obj = json_decode($jsonStr);

        foreach ($obj as $key => $val) {
            $this->{$key} = $val;
        }
    }

    public function __toString()
    {
        return json_encode($this);
    }
}