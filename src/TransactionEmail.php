<?php
/**
 * Defines the TransactionEmail class
 *
 * PHP version 5
 *
 * @author Craig Thayer <cthayer@sazze.com>
 * @copyright 2014 Sazze, Inc.
 */

namespace sz\envoy;

class TransactionEmail extends Email
{
    public function __construct($jsonStr = '')
    {
        parent::__construct($jsonStr);

        $this->headers['X-Envoy-Type'] = 2;
    }
}