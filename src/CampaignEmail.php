<?php
/**
 * Defines the CampaignEmail class
 *
 * PHP version 5
 *
 * @author Craig Thayer <cthayer@sazze.com>
 * @copyright 2014 Sazze, Inc.
 */

namespace sz\envoy;

class CampaignEmail extends Email
{
    public function __construct($jsonStr = '')
    {
        parent::__construct($jsonStr);

        $this->headers['X-Envoy-Type'] = 3;
    }
}